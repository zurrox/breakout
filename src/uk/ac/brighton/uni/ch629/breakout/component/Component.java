package uk.ac.brighton.uni.ch629.breakout.component;

import uk.ac.brighton.uni.ch629.breakout.entity.Entity;
import uk.ac.brighton.uni.ch629.breakout.system.SystemManager;

public abstract class Component {
    private Entity attachedTo = null;

    public Component() {

    }

    public void onAttach(Entity to) {
        SystemManager.getInstance().addComponent(this);
        attachedTo = to;
    }

    public void onDetach(Entity from) {
        SystemManager.getInstance().removeComponent(this);
        attachedTo = null;
    }

    public Entity getEntityAttachedTo() {
        return attachedTo;
    }
}