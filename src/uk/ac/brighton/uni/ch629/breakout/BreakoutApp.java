package uk.ac.brighton.uni.ch629.breakout;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import uk.ac.brighton.uni.ch629.breakout.component.CompTransform;
import uk.ac.brighton.uni.ch629.breakout.entity.Entity;

public class BreakoutApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        Entity test = new Entity();
        test.components.add(new CompTransform());
        if (test.getComponent(CompTransform.class) != null) System.out.println("FOUND");
    }


    public static void main(String[] args) {
        launch(args);
    }
}