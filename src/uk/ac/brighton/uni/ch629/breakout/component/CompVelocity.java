package uk.ac.brighton.uni.ch629.breakout.component;

import javafx.geometry.Point2D;
import uk.ac.brighton.uni.ch629.breakout.system.SysMovement;

@ComponentData(systemToUse = {SysMovement.class})
public class CompVelocity extends Component {
    public Point2D vel;

    public CompVelocity() {
        super();
        vel = new Point2D(0, 0);
    }

    public CompVelocity(double x, double y) {
        vel = new Point2D(x, y);
    }
}