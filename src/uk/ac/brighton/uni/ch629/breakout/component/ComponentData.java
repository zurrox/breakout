package uk.ac.brighton.uni.ch629.breakout.component;

import uk.ac.brighton.uni.ch629.breakout.system.System;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ComponentData {
    Class<? extends System>[] systemToUse();
}