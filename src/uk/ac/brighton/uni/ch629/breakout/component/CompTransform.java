package uk.ac.brighton.uni.ch629.breakout.component;

import javafx.geometry.Point2D;
import uk.ac.brighton.uni.ch629.breakout.system.SysCollisions;
import uk.ac.brighton.uni.ch629.breakout.system.SysMovement;
import uk.ac.brighton.uni.ch629.breakout.system.SysRender;

@ComponentData(systemToUse = {SysMovement.class, SysCollisions.class, SysRender.class})
public class CompTransform extends Component {
    public double width, height;
    public Point2D pos;

    public CompTransform() {
        width = 1;
        height = 1;
        pos = new Point2D(0, 0);
    }

    public CompTransform(int width, int height, double x, double y) {
        this.width = width;
        this.height = height;
        pos = new Point2D(x, y);
    }
}