package uk.ac.brighton.uni.ch629.breakout.system;

import uk.ac.brighton.uni.ch629.breakout.component.Component;
import uk.ac.brighton.uni.ch629.breakout.component.ComponentData;

import java.util.ArrayList;
import java.util.List;

public class SystemManager {
    private static SystemManager instance = new SystemManager();
    private List<System> systems = new ArrayList<>();

    private SystemManager() {
        addSystem(new SysCollisions());
    }

    public static SystemManager getInstance() {
        return instance;
    }

    public void addSystem(System system) {
        if(!systems.contains(system)) systems.add(system);
    }

    public boolean systemExists(Class<? extends System> clazz) {
        for(System sys : systems) if(sys.getClass().equals(clazz)) return true;
        return false;
    }

    public System getSystem(Class<? extends System> clazz) {
        for(System sys : systems) if(sys.getClass().equals(clazz)) return sys;
        return null;
    }

    public void addComponent(Component component) {
        if (component.getClass().isAnnotationPresent(ComponentData.class)) {
            for (Class<? extends System> clazz : component.getClass().getAnnotation(ComponentData.class).systemToUse())
                getSystem(clazz).attach(component);
        }
    }

    public void removeComponent(Component component) {
        if (component.getClass().isAnnotationPresent(ComponentData.class)) {
            for (Class<? extends System> clazz : component.getClass().getAnnotation(ComponentData.class).systemToUse())
                getSystem(clazz).detach(component);
        }
    }
}