package uk.ac.brighton.uni.ch629.breakout.system;

import uk.ac.brighton.uni.ch629.breakout.component.Component;

import java.util.ArrayList;
import java.util.List;

public class SysCollisions implements System {
    private List<Component> components = new ArrayList<>();

    @Override
    public void attach(Component component) {
        components.add(component);
    }

    @Override
    public void detach(Component component) {
        components.remove(component);
    }

    @Override
    public void update() {

    }
}