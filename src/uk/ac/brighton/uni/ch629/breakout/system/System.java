package uk.ac.brighton.uni.ch629.breakout.system;

import uk.ac.brighton.uni.ch629.breakout.component.Component;

public interface System {
    void attach(Component component);
    void detach(Component component);
    void update();
}