package uk.ac.brighton.uni.ch629.breakout.component;

import javafx.geometry.Rectangle2D;
import uk.ac.brighton.uni.ch629.breakout.system.SysCollisions;

@ComponentData(systemToUse = {SysCollisions.class})
public class CompBoxCollider extends Component {
    public Rectangle2D rect;

    public CompBoxCollider() {
        rect = new Rectangle2D(0, 0, 1, 1);
    }

    public CompBoxCollider(double minX, double minY, double maxX, double maxY) {
        rect = new Rectangle2D(minX, minY, maxX, maxY);
    }
}