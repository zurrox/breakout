package uk.ac.brighton.uni.ch629.breakout.entity;

import com.sun.istack.internal.NotNull;
import uk.ac.brighton.uni.ch629.breakout.component.Component;

import java.util.*;

public /*abstract*/ class Entity {
    public static HashMap<UUID, Entity> entityList = new HashMap<>();
    public UUID id;
    public List<Component> components = new ArrayList<>();

    public static Entity getEntity(UUID uuid) {
        if(entityList.containsKey(uuid)) return entityList.get(uuid);
        return null;
    }

    public Component getComponent(@NotNull final Class<? extends Component> clazz) {
        for(Component comp : components) {
            if(comp.getClass().equals(clazz)) {
                return comp;
            }
        }
        return null;
    }

    public boolean hasComponentType(@NotNull final Class<? extends Component> clazz) {
        for(Component component : components) if(component.getClass().equals(clazz)) return true;
        return false;
    }

    public void addComponent(@NotNull final Component comp) {
        if(!hasComponentType(comp.getClass())) {
            components.add(comp);
            comp.onAttach(this);
        }else{
            removeComponent(comp.getClass());
            addComponent(comp);
        }
    }

    public void removeComponent(@NotNull final Class<? extends Component> clazz) {
        Iterator<Component> itr = components.iterator();
        while(itr.hasNext()) {
            Component next = itr.next();
            if(next.getClass().equals(clazz)) {
                next.onDetach(this);
                itr.remove();
            }
        }
    }
}